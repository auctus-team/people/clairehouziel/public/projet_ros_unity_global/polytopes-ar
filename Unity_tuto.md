## Introduction

Unity est une plateforme de développement 3D, utilisée notamment pour la création de jeux vidéos, qui permet le déploiement sur de nombreuses interfaces : appareils mobiles, ordinateurs, dispositifs de réalité augmentée/virtuelle... Il s'agit d'un logiciel informatique développé par Unity Technologies.

## Interface Logiciel

### Les fenêtres

L'interface logiciel comprend 4 fenêtres :

- Project : là où on ajoute les ressources nécessaires pour produire la scène (scripts, textures...)
- Scene : la visualisation de la scène en train d'être créée (2 objets par défaut : caméra et lumière)
- Hierarchy : les objets ajoutés dans la scène et leurs relations entre eux
- Inspector : les propriétés des différents objets ajoutés et les scripts associés

### Les GameObjects

La scène est constituée d'objets appelés des "GameObjects", qui ont différentes propriétés, modifiables et ajoutables. Par exemple :

- Transform : définit la position/rotation de l'objet dans la scène
- Components :
  * Sprite Renderer : définit l'aspect (via un png par exemple)
  * Rigid body : ajoute une masse
  * Material : définit le matériau de l'objet
  * Scripts à écrire soi-même

On les ajoute en cliquant droit sur la fenêtre Hiérarchie et en choisissant leur forme. Tout script créé doit être associé à un GameObject pour être actif dans la scène, le plus simple est de créer un GameObject empty (c'est-à-dire qu'il n'aura pas de représentation graphique) et de lui ajouter le script en ajoutant un component.

On peut déplacer ces objets directement dans la scène en cliquant sur l'objet puis en maintenant appuyé sur une des flèches tout en faisant glisser la souris, ou bien en jouant directement sur les valeurs de position et de rotation (dans la fenêtre Hierarchy > rubrique Transform).

On peut associer des GameObjects dans des arbres hiérarchiques (sous forme de parents et childs) dans la fenêtre Hierarchy. Cela marche comme dans un URDF, c'est à dire que les positions des childs seront relatives aux positions des parents. On peut enregistrer cet arbre en "Prefab" pour les réutiliser de manière globale. Pour ce faire, on drag and drop la racine de la fenêtre Hiérarchie à la fenêtre Projet, puis on pourra réutiliser le Prefab en l'ajoutant directement dans la Hiérarchie.

### Système de coordonnées

Dans Unity, le système de coordonnées est un système "main gauche", avec l'axe y vers le ciel, l'axe . On distingue le world space du local space :

* world space : système de coordonnées de la scène en général
* local space : système de coordonnées relatif à la position et rotation d'un objet spécifique

### Script en C#

Unity fonctionne avec des scripts codés en C#. Lorsqu'on ajoute un nouveau script à un GameObject, la base du code est déjà écrite avec les 2 étapes : Start et Update. On peut alors modifier directement des propriétés de cet objet dans le script, en faisant référence à l'objet en question via le nom de classe "gameObject".

On peut faire communiquer ce script avec d'autres components de l'objet en ajoutant des références au sein de la classe (par exemple "public RigidBody2D myRigidBody" ). On peut également modifier la position de l'objet :

* dans Update : transform.position = transform.position + (Vector3.left \* moveSpeed \* Time.deltaTime) (par exemple pour faire bouger vers la gauche un objet, avec Time.deltaTime qui permet de s'affranchir de la vitesse de chaque ordinateur).

On peut utiliser des références à d'autres GameObjects de la scène en ajoutant la référence au sein de la classe "public GameObject nomdugameobject" : cela ajoutera une rubrique dans l'Inspector dans laquelle on pourra drag and drop de la fenêtre Hiérarchie l'objet qui nous intéresse.

### Interface utilisateur

On peut créer une interface utilisateur sur l'application de l'Hololens :

* Dans la Hierarchy, on crée un nouvel object UI : text
* Pour ensuite faire un manager et appeler le texte, on importe d'abord "using UnityEngine.UI" puis dans la classe on appelle "public Text \[nomdutexte dans le script\]" (ensuite il faudra drag et drop l'objet texte sur la variable texte du manager)

### Packages

Différents packages peuvent être ajoutés au projet pour ajouter des fonctionnalités : il peut s'agir de packages disponibles sur :

* GitHub (comme par exemple le ROS-TCP Connector qui sera expliqué plus bas) : soit on passe par le Package Manager et on ajoute le lien git ou bien on télécharge le dossier zip puis on l'exécute en double-cliquant dessus directement avec le projet Unity ouvert.
* Manager de Packages NuGet (qui est un Plugin de Unity) : on peut directement chercher le nom du package NuGet et l'installer.

## Architecture Dossier

Le dossier du projet réalisé sur Unity va être composé de différents sous-dossiers organisés de telle sorte :

* Assets : c'est là où se trouvent toutes les ressources du projet (Prefab, Scripts, URDF, Scènes...)
* Packages : les différents packages ajoutés au projet
* Project Settings : c'est là où seront enregistrés les paramètres

D'autres dossiers seront ajoutés à la compilation du projet (Library, Temp...) mais peuvent se reconstruire même après une suppression, en chargeant de nouveau le projet dans Unity.

Chaque dossier et fichier est accompagné d'un fichier .meta nécessaires à Unity pour charger la configuration telle qu'elle était à la dernière session. Ils peuvent être supprimés, ils seront recompilés à chaque session.

## ROS et Unity

Unity et ROS peuvent communiquer au travers de packages permettant de faire le pont entre les deux applications, comme ROS Bridge ou le ROS-TCP Connector. Dans ce projet, la deuxième option a été utilisée.

### ROS-TCP Connector

Via l'installation du package [ROS TCP Endpoint](https://github.com/Unity-Technologies/ROS-TCP-Endpoint%7D%7BROS) dans le workspace ROS du projet et l'installation du package ROS-TCP Connector (Package Manager > Add from git : https://github.com/Unity-Technologies/ROS-TCP-Connector.git?path=/com.unity.robotics.ros-tcp-connector) .

Cela permet du côté de ROS d'avoir un serveur qui récupère les messages de Unity et qui les envoie aux différents nodes, et inversement. Du côté Unity, cela permet d'avoir un package qui convertit les messages ROS vers Unity et inversément, et un package qui fournit les scripts pour subscribe, publish ou appeler un service ROS (voir le schéma en fin de partie).

### Robot & Unity

Pour créer un robot sur Unity, il faut utiliser des fichiers .urdf (à ajouter dans le dossier Assets du projet) grâce au package [URDF-Importer](https://github.com/Unity-Technologies/URDF-Importer%7D%7BURDF) à ajouter au projet.

Si le fichier est un xacro, la conversion est possible via la commande :

`rosrun xacro xacro --inorder -o PATH/TO/panda_arm.urdf PATH/TO/panda_arm.urdf.xacro`

Ici, on utilise le panda_arm.urdf présent dans le dossier URDF.

Pour l'ajouter à la scène, cliquer-droit sur la Hiérarchie > 3D Object > URDF Model (import). Attention, le repère de référence a son axe y vers le ciel (et non l'axe z).

Quand on utilise un script pour déplacer la base du robot, la physique de Unity ne permet pas de déplacer les articulations en même temps (les liaisons ArticulationBody font appel au modeleur physique de Unity ce qui contraint leur déplacements, autre que par la physique). Il faut alors utiliser la fonction _TeleportRoot(pose.position, pose.rotation)_. On demande l'articulation de base en début de script puis on ajoute une ligne dans la partie Update : _baseLink.TeleportRoot(pose.position, pose.rotation)_

### Les polytopes dans Unity

Pour qu'ils soient bien calés, utiliser dans l'inspector du PolytopeSuscriber : parentobject = panda_link0

Pour cacher le robot : utiliser un layer sur le panda dans l'inspector (ici, j'utilise le layer 2 : Ignore Raycast, mais ce n'est qu'un nom arbitraire). Puis cliquer sur la MainCamera > Inspector > Camera > Culling mask : décocher le layer sélectionné (la caméra ne verra donc plus les objets qui ont ce layer dans le game, mais on le verra toujours dans la scène)

Attention, obligation de modifier les matériaux dans le DrawingManager car ceux-ci ne sont pas supportés en stéréo (on ne les voit que sur l'oeil gauche de l'Hololens). Possibilité de modifier par les matériaux classiques visibles dans le Project > All Materials.

### Créer de nouveaux scripts liés à ROS

Nécessite d'appeler `using Unity.Robotics.ROSTCPConnector;` en début de script, mais aussi le type de message qui va être écouté (par exemple : `using RosMessageTypes.Geometry;`). On peut trouver le nom des types de messages dans le dossier du projet : Packages > com.unityrobotics.roc-tcp-connector > Runtime > Messages.

On ajoute dans la public class le nom du topic que l'on écoute : `[SerializeField] string topicName = "/panda/polytope";` et la connexion ROS `private ROSConnection rosConnection;`

Dans la rubrique Start, on démarre la connexion avec ROS via :

`rosConnection = ROSConnection.GetOrCreateInstance();`

`rosConnection.Subscribe<TwistMsg>(topicName, ReceiveVector);` (attention à bien modifier le type de message dans le subscriber selon ce que vous utilisez, et le ReceiveVector est un genre de callback.

On définit ensuite le callback :

`private void ReceiveVector(TwistMsg twistMsg)`

`{`

`linearVelocity = new Vector3((float) - twistMsg.linear.y, (float) twistMsg.linear.z, (float) twistMsg.linear.x);`

`angularVelocity = new Vector3((float) twistMsg.angular.x, (float) twistMsg.angular.z, (float) twistMsg.angular.y);`

`}`

Et on ajoute des méthodes publiques pour pouvoir accéder aux données à l'extérieur de ce script. Par exemple:

`public Vector3 LinearVelocity`

`{`

`get { return linearVelocity; }`

`}`

## Hololens et Unity

L'Hololens 2 est un casque de réalité augmentée de Microsoft, et il est possible de construire des applications sur ce dispositif via Unity. Pour les faire communiquer, il est nécessaire d'utiliser l'Universal Windows Platform, plugin qui fournit une plateforme d’application commune sur chaque appareil exécutant Windows. C'est pourquoi il faut utiliser Unity sur Windows.

Il faudra alors utiliser le package MixedRealityToolKit qui fournit des fonctionnalités pour les applications de Réalité Augmentée (par exemple, le suivi du regard ou l'analyse de l'environnement).

Il existe alors deux façons de visualiser l'application de Unity sur l'Hololens : l'Holographic Remoting et la construction de l'application.

### Holographic Remoting

On peut utiliser un streaming de Unity directement sur l'Hololens. Pour cela :

* Sur l'Hololens : dans le Menu > All Apps > Holo Remoting. Une page d'attente de connexion s'affiche avec l'adresse IP de l'appareil.
* Sur Unity : il faut d'abord autoriser le Remoting via MixedReality > Remoting > Holographic Remoting for Play Mode. Une fenêtre s'ouvre, on ajoute alors le Remote Host Name (172.16.0.183) et on clique sur la barre "Enable Holographic Remoting for Play Mode". A partir de là, chaque fois qu'on clique sur Play, cela fonctionnera sur l'Hololens (qui doit être sur la page d'attente de connexion pour fonctionner). Si on veut repasser en mode Play normal, on retourne sur la fenêtre de Unity et on clique sur "Disable".

### Build on Hololens

Ici, on crée une application directement sur l'Hololens, on n'a plus besoin d'être connecté à Unity pour utiliser l'application.

Pour cela, deux méthodes :

* Via le wifi (vérifier que l'ordinateur et l'Hololens sont sur le réseau crur) : [Suivre ce lien pour le tutoriel détaillé](https://learn.microsoft.com/en-us/training/modules/learn-mrtk-tutorials/1-7-exercise-hand-interaction-with-objectmanipulator%7D%7BSuivre)
  * Dans Unity, aller dans Files > Build Settings (vérifier que ce soit bien sur la plateforme UWP) puis faire Build. Créer un dossier nommé "Builds" et sélectionner ce dossier.
  * Ensuite le dossier s'ouvre. Double-cliquer sur PandaCapacityAR.sln. Visual Studio 2019 s'ouvre. Sur la barre d'outils du haut, modifier "Debug" en "Release" et "ARM" en "ARM64".
  * Aller dans Project > Properties. Dans Configuration Properties > Debugging, ajouter le machine name (172.16.0.183).
  * Puis aller dans la barre d'outils Debug > Start without debugging (avec l'Hololens allumé)
* Avec le [câble USB](https://learn.microsoft.com/en-us/windows/mixed-reality/develop/unity/use-pc-resources%7D%7Bc%C3%A2ble) : on applique les mêmes modifications mais dans la barre d'outils du haut, on remplace Remote Machine par "Device".

## Autres : Les erreurs dans Unity

* Ne pas hésiter à changer de version de Unity Editor
* Si erreur "'Texture2D' does not contain a definition for 'Reinitialize' and no accessible extension method 'Reinitialize' accepting a first argument of type 'Texture2D' could be found" : aller dans le script en double cliquant sur l'erreur et remplacer Reinitialize par Resize
* Si erreur "An error occurred while resolving packages: Project has invalid dependencies: com.unity.feature.development: Package \[com.unity.feature.development@1.0.1\] cannot be found. A re-import of the project may be required to fix the issue or a manual modification of C:/Users/claire/Documents/pandaarASA/pandaarASA/Packages/manifest.json file.": supprimer le fichier manifest.json du dossier Packages (il sera automatiquement régénéré par Unity.
* Quand il y a des "clones" pendant la simulation (par exemple les QR Codes), utiliser les tags pour pointer les gameobjects (GameObject\[\] qrCodeObjects = GameObject.FindGameObjectsWithTag(qrCodeTag);)
