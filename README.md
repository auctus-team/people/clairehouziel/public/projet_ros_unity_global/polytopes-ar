# Organisation de la documentation

Projet global :
- du côté Unity : ci-dessous
- [du côté ROS](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projet_ros_unity_global/polytope_ar_ros/-/blob/master/README.md) 

Décomposition en sous-projets : 
- du côté Unity 
    - [sous-projet sur les polytopes](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projets_unity/hololens_polytopes/-/blob/master/README.md) 
    - [sous-projet sur le vecteur vitesse](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projets_unity/hololens_vecteur_vitesse/-/blob/master/README.md) 
    - [sous-projet sur les objets d'intérêt](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projets_unity/hololens_objects/-/blob/master/README.md) 
- du côté ROS 
    - [scripts à ajouter aux sous-projets](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity/-/blob/master/README.md) 

Documentation technique : 
- [Prise en main de Unity](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projet_ros_unity_global/polytopes-ar/-/blob/master/Unity_tuto.md) 
- [Déroulement du projet ](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projet_ros_unity_global/polytopes-ar/-/blob/master/Descriptif_projet.md)



# Aperçu global du projet

Globalement, ce projet est constitué de : 
- d'un projet Unity "Polytopes_AR_Unity" avec :

    - le repository [PandaAR](https://gitlab.inria.fr/auctus-team/people/antunskuric/pandaar) de Mark

    - le repository [QRCodeTracking](https://github.com/microsoft/MixedReality-QRCode-Sample) de Microsoft (_les Assets et les Prefabs ont été ajoutés dans Assets > Projet > QRCode_ )
    - un lien entre les deux réalisé par le GameObject RootQRCode avec le script MoveRobot.cs (qui récupère la position et l'orientation du QR Code et l'associe à ce GameObject) : 
        - tous les éléments de visualisation sont alors des childs de cet objet, avec des transformations différentes selon la modalité (panda, vecteur vitesse et cubes)



- d'un projet ROS "[Polytopes_AR_ROS](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projet_ros_unity_global/polytope_ar_ros)" avec :
    - le package [panda_capacity](https://gitlab.inria.fr/auctus-team/people/antunskuric/ros/ros_nodes/panda_capacity) d'Antun (pour le calcul et l'affichage des polytopes)
    - le package franka_ros du workspace [panda_qp_ws](https://gitlab.inria.fr/auctus-team/components/robots/panda/panda_qp_ws) de Lucas
    - le repository [setup_calibration](https://gitlab.inria.fr/auctus-team/projects/internal/lego_manipulation/setup-calibration/-/tree/master/) de Lucas pour calibrer le robot par rapport au QR Code
    - le repository [setup_description](https://gitlab.inria.fr/auctus-team/projects/external/solvay/glovebox/solvay_setup_description) en complément du précédent
    - le repository [ROS TCP Endpoint](https://github.com/Unity-Technologies/ROS-TCP-Endpoint), permettant la communication avec Unity 
    - le repository [panda_unity](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/package_ros/panda_unity) qui permet la visualisation des vecteurs vitesses et la réception des coordonnées des cubes



# Installation de l'environnement 

Pour le projet de visualisation des polytopes en réalité augmentée via Hololens 2, il faut :
-  un PC Linux pour faire fonctionner ROS 
- un PC Windows pour faire fonctionner Unity en lien avec l'Hololens. 

## PC Linux

- Installation de ROS 1 version Noetic : [Guide d'installation officiel](http://wiki.ros.org/noetic/Installation/Ubuntu), avec  [vidéo tutorielle](https://www.youtube.com/watch?v=Qk4vLFhvfbI) non officielle.

- Installation du workspace [Polytope_AR_ROS](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projet_ros_unity_global/polytope_ar_ros.git)

```bash
git clone https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projet_ros_unity_global/polytope_ar_ros.git
```

**Interface ROS-Unity :** (cette partie est déjà faite dans le projet)
- intégrer le package [ROS TCP Endpoint](https://github.com/Unity-Technologies/ROS-TCP-Endpoint) dans le workspace ROS (panda_capacity_ws)
```bash
cd panda_capacity_ws/src
git clone https://github.com/Unity-Technologies/ROS-TCP-Endpoint.git
```

- dans le fichier launch de ce package (endpoint.launch), modifier la valeur de "tcp-ip" pour l'adresse IP de la machine utilisée (obtenue grâce à "\$ ifconfig" dans le terminal, section "inet") 

- copier-coller l'intérieur de ce fichier launch dans le fichier launch de panda\_capacity (one\_panda.launch) 




## PC Windows

### Visual Studio

- Installation de [Visual Studio 2019](https://visualstudio.microsoft.com/fr/vs/older-downloads/) et ajout dans Visual Studio Installer > Modify de :

- [ ] Développement Desktop en C++

- [ ] Développement pour la plateforme Windows universelle 

- [ ] Développement de jeux avec Unity

- [ ] Connectivité des périphériques USB


### Unity

- Installation de [Unity Hub](https://unity.com/fr/download) après avoir fait un compte (gratuité si étudiant). 

- Installation de [Unity Editor](https://unity.com/releases/editor/archive) version 2021.3.18, disponible dans les archives. 

- Installation de [Universal Windows Platform](download.unity3d.com/download_unity/3129e69bc0c7/TargetSupportInstaller/UnitySetup-Universal-Windows-Platform-Support-for-Editor-2021.3.18f1.exe) pour cette version de Unity Editor (sinon aller dans Unity Editor > Fichier > Build Settings > Universal Windows Platform puis "Open Download Page")

- Installation de [NuGet](https://www.nuget.org/downloads) (plugin d'Unity, gestionnaire de packages)

- Installation du [MixedRealityToolKit](https://www.microsoft.com/en-us/download/details.aspx?id=102778), qui permet de créer des applications de réalité augmentée. 

- Installation de [MixedRealityQR](https://www.nuget.org/Packages/Microsoft.MixedReality.QR) pour la reconnaissance des QR (qui est un package Nuget) via la barre d'outils NuGet > Manage NuGet Packages puis taper "Microsoft.MixedReality.QR" dans la barre de recherche et cliquer sur install 


# Projet :

- Télécharger ce package sous forme d'un fichier zip puis le décompresser

- Ouvrir ce projet dans Unity Hub

- Dans la barre de recherche de la fenêtre "Projet", chercher "MainScene" et double-cliquer pour afficher le robot. 

- Dans File > Build Settings : changer la plateforme vers Universal Windows Platform et changer l'architecture vers "ARM64"


_Interface ROS-Unity :_ 
- dans Unity, via l'onglet Robotics > ROS Settings, modifier "ROS IP Adress" pour l'adresse IP de la machine Linux utilisée. 


## Hololens

Pour afficher la scène de Unity sur l'Hololens : 2 possibilités, nécessitant toutes les deux que le PC Windows et l'Hololens soient connectés au __même__ réseau WiFi. 

### Via l'Holographic Remoting

- Sur l'Hololens : afficher le menu Démarrer. Aller dans "All Apps" et cliquer sur Holographic Remote. Un cube apparaît, cliquer sur le logo Play. L'adresse IP de l'appareil apparaît alors. 

- Du côté d'Unity : faire Mixed Reality > Remoting > Holographic Remoting for Play Mode. Inscrire l'adresse IP de l'Hololens, puis appuyer sur "Enable Holographic Remoting for Play Mode". Puis appuyer sur le logo Play au dessus de la scène. 

### Via la construction de l'application sur l'Hololens 

- Dans Unity > File, cliquer sur Build Settings (vérifier que ce soit bien sur la plateforme UWP) puis sur Build en bas à droite de la fenêtre qui s'ouvre : une fenêtre d'enregistrement s'ouvre, créer un nouveau dossier Builds et le sélectionner. 

- Une fois que la compilation est terminée, la fenêtre s'ouvre : double-cliquer sur PandaCapacityAR.sln. 
- Visual Studio 2019 s'ouvre : 
    - Sur la barre d'outils du haut, modifier "Debug" en "Release" et "ARM" en "ARM64". 
    - Aller dans Project > Properties. Dans Configuration Properties > Debugging, ajouter le machine name (172.16.0.183). 
    - Aller dans la barre d'outils Debug > Start without debugging (avec l'Hololens allumé) : l'application s'ouvrira spontanément à la fin de la procédure. 


# Les différentes visualisations

L'organisation dans Unity (dans la fenêtre Hierarchy) : 
- le ROS Manager : il contient tous les scripts d'interaction avec ROS (les subscribers des joint_states, des polytopes, du vecteur vitesse, le gestionnaire graphique pour l'affichage du polytope)

- le QRCodeManager : il contient les scripts permettant le tracking des QRCodes et de déterminer leurs coordonnées dans le repère de Unity. 

- le RootQRCode : il s'agit du GameObject parent de toutes les modalités de visualisation. Son script "MoveRobot.cs" récupère les coordonnées du QRCode repéré et se les attribue automatiquement. 

    - le panda : formé via un URDF, il est placé par une transformation via le script MoveRobot également et il se configure grâce au ROS Manager par le script JointSubscriber. 

    - les cubes

    - le vecteur vitesse

### Retrait du robot 

- Pour ne plus voir le robot, dans la fenêtre Hierarchy, cliquer sur RA > MixedReality Playspace > Main Camera. Dans la fenêtre Inspector, dans la rubrique Camera, aller dans Culling Mask : décocher IgnoreRayCast (qui est le layout du Panda, tout à fait modifiable)


## Les polytopes

Les polytopes sont publiés via le script PolytopeSubscriber (présent dans Assets > Projet > Scripts) qui reçoit les messages ROS du topic de polytope qu'on lui attribue (c'est-à-dire qu'on peut choisir via l'interface Unity celui que l'on veut afficher). 

Par défaut, il s'agit du velocity_subscriber et il est publié en rouge. 

### Modification de polytope

- Pour changer de type de polytope, dans la fenêtre Hierarchy, cliquer sur ROSManager > VelocityPolytopeSubscriber, puis dans la fenêtre Inspector, rubrique Script : modifier le topic name (par exemple, /panda/force_polytope)

- si problème de positionnement du polytope, vérifier dans la même rubrique que le parent_obj est sur panda_link0. Si ce n'est pas le cas, faire un drag-and-drop directement de la fenêtre Hierarchy du panda_link0 vers le parent_obj de VelocityPolytopeSubscriber. 


- Pour ne plus voir le polytope, dans la fenêtre Hierarchy, cliquer sur ROSManager > VelocityPolytopeSubscriber, puis dans la fenêtre Inspector, décocher la case à côté du nom 


### Modification graphique 

- Pour changer l'aspect du polytope, dans la fenêtre Hierarchy, cliquer sur ROSManager > Drawing Manager. Dans la fenêtre Inspector, dans la rubrique Script, modifier les matériaux des deux premières lignes (en cliquant sur le bouton à droite)


## Vecteur vitesse de l'effecteur

Le vecteur vitesse est formé de deux objets, reliés par un segment de droite :

- l'objet Trajectoire, composé du script "TrajectoireDisplay" qui récupère la position et l'orientation de l'effecteur par rapport au QR Code et se les attribue lui-même. 

- l'objet Arrow (sans représentation graphique), composé du script ArrowVector, qui récupère les messages ROS donnant le Twist du vecteur vitesse de l'effecteur par rapport au repère de l'effecteur, et qui attribue les coordonnées de la vitesse linéaire pour ses propres coordonnées.

- le LineRenderer qui trace un segment entre ses deux objets. 

### Modification graphique 

- Pour ne plus voir le vecteur : dans la fenêtre Hierarchy, cliquer sur RootQRCode > Trajectoire, puis dans la fenêtre Inspector, décocher la case à côté du nom (cela décochera le LineRenderer et l'Arrow également)

- Pour changer l'aspect visuel :

    - possibilité de modifier l'échelle du vecteur directement dans le script de l'objet Arrow (de base, les valeurs sont multipliées par 3)

    - possibilité de modifier la couleur et l'aspect du vecteur en cliquant sur LineRenderer, dans la fenêtre Inspector via Color et Width

## Les objets d'intérêt 

Un GameObject "Cubes" sans représentation graphique regroupe les différents cubes, et est associé au script CubePositionPublisher qui permet :
 - de publier en message ROS les coordonnées du cube le plus proche de l'effecteur, dans le repère de la base du robot (coordonnées converties en repère right-handed)

 - de modifier la couleur du cube le plus proche de l'effecteur si celui-ci est à un certain seuil de distance

 ### Comment ça marche

 Les cubes ont le tag "cube" (à faire manuellement si jamais vous voulez en ajouter : dans la fenêtre Inspector du cube, modifier le tag sous le nom). Le script cherche ensuite tous les objets de la scène qui ont ce tag et calcule la distance de l'effecteur avec chacun. 

 ### Modification graphique 
 - Pour ne plus voir les cubes : dans la fenêtre Hierarchy, cliquer sur RootQRCode > Cubes, puis dans la fenêtre Inspector, décocher la case à côté du nom (cela décochera tous les cubes children) 

 - Pour modifier leur aspect initial : dans chaque cube, dans la fenêtre Inspector, rubrique MeshRenderer : modifier  Materials

 - Pour modifier l'aspect à l'approche de l'effecteur : dans le GameObject Cubes, dans la fenêtre Inspector, dans la rubrique du Script, modifier le TargetMaterial

 - Possibilité également de changer le seuil de distance dans cette même fenêtre ou directement dans le script

