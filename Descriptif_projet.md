# Contexte et état de l'art actuel

Les interactions humain-robot sont de plus en plus fréquentes, du fait de l'émergence et la prévalence croissante des robots dans les espaces de travail. Il est nécessaire d'optimiser l'efficacité et les performances de chaque agent, et pour cela, la compréhension rapide et fluide des intentions de chacun est primordiale afin de pouvoir partager en toute sécurité l'espace de travail. Les humains utilisent des techniques verbales ou non verbales de type axe du regard ou gestes spécifiques qui sont communément admises, cependant il n'est pas toujours possible de les adapter à des agents robotisés, notamment pour des bras robotiques. Il est ainsi nécessaire de réfléchir à des signaux utilisables par des robots et intuitifs pour les humains pour travailler au mieux de manière collaborative.

Ce sujet est de plus en plus étudié, comme en attestent les nombreux articles qui sont publiés depuis quelques années. Une revue de la littérature récente montre que différents signaux ont pu être évalués au fil des études, selon le rôle de l'humain par rapport au robot (qui a pu être qualifiée de collaborateur, observateur, collègue ou spectateur), l'intention du signal (partager sa trajectoire, attirer l'attention, informer sur l'état ou bien donner une instruction), et le type de robot (bras robotique, robot humanoïde ou robot mobile). [1]

Dans ce projet, nous nous intéresserons plus particulièrement au bras robotique, souhaitant apporter l'information de son intention de mobilité. Dans ce domaine, une étude a étudié différentes modalités d'avertissement sur un mouvement à venir : elle a pu montrer qu'un signal près de l'effecteur est le plus visible et le moins confusiogène pour les humains, notamment via un bracelet LED, même s'il reste d'une compréhensibilité limitée [2].

Dans l'esprit d'agents mobiles dans les trois dimensions, les recherches se sont également portées sur les drones. Un article a testé un bracelet LED installé autour de la structure centrale du drone, utilisant l'éclairage spécifique de certaines portions du bracelet en fonction de la direction prise par le robot.[3]

Des systèmes de projection au sol, qu'il s'agisse de flèches ou de bandes lumineuses mimant le trajet futur, ont été utilisés pour les robots mobiles ([4-7]), mais pas pour les bras robotiques. En effet dans ce cas, le signal serait très éloigné de l'effecteur et apporterait des informations à un endroit supplémentaire, ce qui risquerait de diminuer l'efficacité de l'agent humain. Le seul article présentant un système de projection pour un bras robotisé utilise une visualisation constante de l'espace de travail du robot au sol, mais a seulement présenté la preuve de concept et n'a pas formellement testé cette méthode sur utilisateur. [8]

L'émergence de la réalité augmentée a ouvert de nouvelles portes pour ce sujet, et actuellement, la grande majorité des études nouvellement parues s'intéressent à cette technique. Différentes modalités ont pu être testées : l'affichage des différentes positions du bras robotiques à intervalle régulier de temps [9], l'affichage des coordonnées des positions et des vélocités articulaires [10], la visualisation du volume atteignable par l'effecteur sous forme de diagramme centrée sur la 1ère articulation [11], l'affichage de boules vertes sur la trajectoire de l'effecteur [12], la visualisation du trajet par ligne et flèches dynamiques [13], ou encore l'affichage de polytope de mobilité centré sur l'effecteur [14].

La comparaison par rapport à un affichage 2D sur tablette et sans visualisation a montré une augmentation de précision et une diminution de temps de complétion de tâche [9]. Des études ont pu comparer des modalités différentes pour trouver la plus claire et la plus intuitive pour l'humain : la première comparait l'affichage de points de navigation, de flèches et d'un regard virtuel sur le robot et a conclu à la supériorité des points de navigation [15] ; tandis que la deuxième concluait à la performance d'un chemin de navigation signalisé par des cônes de part et d'autre de flèches centrales [16].

Ainsi, de nombreux signaux ont pu être étudiés, avec une nette tendance à la réalité augmentée sur les dernières années. Les travaux récents de l'équipe de recherche AUCTUS s'intéressent à la construction virtuelle de polytopes représentant les capacités maximales d'un bras robotique en terme de force, de vélocité, d'accélération et d'espace de travail atteignable [17]. Le but du projet de ce stage était donc de combiner ces deux aspects en implémentant la visualisation des polytopes en réalité virtuelle sur le bras robotique avec une adaptation en temps réel aux poses articulaires du robot.

# Méthode

## Matériel

Ce projet requiert différents dispositifs : 

* un bras robotique de modèle Panda de la marque Franka Emika
*  le projet "panda_capacity" développé sur ROS par Antun Skuric, permettant le calcul des polytopes du robot
* le logiciel Unity nécessaire à la création d'application sur Hololens (l'édition utilisée est la 2021.3.18f)
* un casque de réalité virtuelle Hololens 2 de Microsoft.

 Afin d'assurer un fonctionnement global et simultané de ces différentes structures, différentes interfaces de communication sont utilisées : 

* l'interface Panda-ROS via le package "franka-ros" fourni par Franka Emika 
* l'interface ROS - Unity par le ROS-TCP-Connector 
* l'interface Unity-Hololens par Universal Windows Platform

Le projet Unity avait été commencé par le chercheur américain Mark Zolotas, permettant alors de visualiser un hologramme du robot associé à son polytope de vélocité, adaptatif selon les positions articulaires commandées virtuellement sur rviz. Déployé sur l'Hololens, l'application montrait l'hologramme du robot à une position déterminée par celle du casque à chaque lancement de l'application, donc variable entre chaque session. 

L'objectif est donc de déterminer une méthode pour ancrer l'hologramme à une position fixe de l'espace réel, quelque soit la position du casque au lancement de l'application. Il s'agit donc d'obtenir la transformation 3D du cadre de référence de l'Hololens  vers le monde réel.

## Méthodes d'ancrage

L'idée est d'utiliser un objet fixe de l'environnement pour déterminer la matrice de transformation du monde virtuel vers le monde réel pour obtenir d'une part $^{reel}T_{virtuel}$. Cela permet ainsi d'obtenir d'une part la matrice de transformation de l'objet dans le repère du monde réel $^{objet}T_{reel}$ , d'autre part la matrice de transformation de l'objet dans le repère du monde virtuel $^{objet}T_{virtuel}$ , et ainsi de pouvoir calculer :

$$ {^{reel}T_{virtuel}} = {^{objet}T^{-1}_{reel}} {^{objet}T_{virtuel}} $$

### Azure Spatial Anchors

Azure est un service cloud de Microsoft, prévu pour simplifier la construction d'applications. Les Azure Spatial Anchors représentent un outil de développement permettant les expériences de réalité augmentée multiusagers et avec conscience de l'environnement. 

L'appel à ce service permet ainsi d'enregistrer sur le cloud une représentation géométrique de l'environnement sous forme d'un nuage de points, via les capteurs de réalité augmentée avec perception par caméra de l'environnement en 6 degrés de liberté. Il est ensuite possible de créer des points d'intérêt sous forme d'ancre qui pourront être réutilisées de session en session sur les appareils connectés à ce cloud. 

C'est un service qui est utilisé dans les constructions d'application, mais qui a comme inconvénient d'être dépendant de Microsoft avec une documentation peu claire et qui devient payant au terme d'un délai de quelques mois. Cela n'a donc pas été la technique adoptée dans ce projet.

### QR Code Tracking

 L'Hololens 2 permet une bonne détection des QR Codes, notamment pour accéder à des liens internet sur l'appareil directement en scannant le QRCode. Microsoft a développé un projet Unity de suivi de QR Code, permettant de déterminer sa position et son orientation, et effectuant la transformation de ces coordonnées du repère de l'Hololens vers le repère de Unity. C'est la technique qui a été utilisée dans ce projet.

## Technique utilisée

### Identification de la transformation du QR Code dans le monde réel

Un QR Code de 10x10cm est placé à proximité immédiate de la base du bras robotique. Une calibration est réalisée par le pointage des 4 coins du QR Code puis la détermination de la position et orientation du coin gauche par rapport au repère de la base du robot réel par la méthode des moindres carrés.

### Identification de la transformation du QR Code dans le monde virtuel

Le projet de Microsoft permet de récupérer les coordonnées du QR Code dans le repère de référence de Unity. L'origine du monde virtuel est alors fixé au coin gauche du QR Code. 

### Conversion des repères Unity et ROS 

Le repère de référence de Unity n'est pas orthonormé : il s'agit d'un repère "left-handed" comme souvent utilisé dans les logiciels de création de jeux vidéos. Cela complique les transformations entre des repères orthonormés définis dans les packages ROS et les repères de l'application créée.

La correspondance des axes est comme suit : 

|        | ROS    | Unity |
| ------ | ------ | ------|
| Devant |   x    |   z   |
| Gauche |   y    |  -x   |
|  Haut  |   z    |   y   |

Ainsi pour convertir des coordonnées ROS en Unity, il est nécessaire de convertir :
- les positions en (-y,z,x) _via_ _la_ _correspondance_ _des_ _axes_
- les quaternions en (y,-z,-x,w) : _on_ _fait_ _la_ _correspondance_ _des_ _xyz_ _et_ _on_ _les_ _négative_ _à_ _cause_ _de_ _la_ _conversion_ _main_ _droite_-_main_ _gauche_. 

Et pour convertir des coordonnées Unity en ROS, il est nécessaire de convertir : 
- les positions en (z,-x, y) _via_ _la_ _correspondance_ _des_ _axes_
- les quaternions en (-z, x,-y, w) : _on_ _fait_ _la_ _correspondance_ _des_ _xyz_ _et_ _on_ _les_ _négative_ _à_ _cause_ _de_ _la_ _conversion_ _main_ _gauche_-_main_ _droite_. 

### Transformation finale 

Le robot est alors placé par rapport au coin gauche du QR Code grace à un script en C#. 
On applique ensuite une rotation de 90° en x puis de 180° en y (en base courante) sur le robot virtuel par rapport au repère de référence de Unity, pour obtenir le robot perpendiculaire au QR Code. 



### Rendu final

Ainsi, pour commencer une session de l'application, un scan du QR Code via l'Hololens est nécessaire à environ 30 cm de hauteur pour qu'il soit bien détecté, puis l'hologramme s'affiche à l'endroit requis.

L'hologramme du robot se superpose au robot réel avec un décalage qui reste faible. Le QR Code reste bien localisé jusqu'à une distance de 5m, au delà il nécessite d'être scanné de nouveau. De même, si l'application reste ouverte avec un retrait puis remise sur la tête de l'Hololens, le QR Code nécessite d'être scanné pour être bien placé. 

 L'hologramme suit de manière efficace les changements de positions articulaires du robot réel, avec un décalage temporel léger.


## Les différentes visualisations 

### Les polytopes 

Les différents polytopes (vitesse, force, accélération) peuvent être observés au niveau de l'effecteur avec une adaptation en temps réel à la configuration du robot. Ils sont calculés par le repository panda_capacity d'Antun. 

### Le vecteur vitesse 

La visualisation du vecteur vitesse a été créée également en utilisant : 
-  le calcul via un node ROS du vecteur vitesse de l'effecteur dans le repère de l'effecteur :
<div style="text-align:center;"> $$ \dot{x} = J \dot{q} $$ </div> 



- un script Unity subscriber, et plaçant le vecteur en temps réel. 


### Les objets d'intérêt 

Des cubes virtuels sont placés sur la table à proximité du robot. 
Un script Unity calcule en temps réel la distance entre l'effecteur et chaque cube, sélectionne le cube le plus proche et publie ses coordonnées par rapport à la base du robot sur un topic ROS. 
En parallèle, il colore en vert le cube le plus proche si l'effecteur se rapproche à moins de 35 cm. 



## Limites rencontrées

- Unity est un logiciel instable, avec de très nombreuses versions quant à la plateforme de développement, qui ne fonctionnent pas de la même manière. En effet, certaines fonctionnalités marchent sur des versions de Unity mais pas sur d'autres, et cela n'est pas explicité. On peut retrouver sur les forums certaines versions à éviter mais aucun communiqué n'est réalisé directement par Unity. De plus, Unity présente des erreurs de compilation fréquentes qui ne sont pas toujours clairement comprises par les programmeurs. 

- L'hologramme reste décalé malgré les modifications de valeurs de translation, et paraît être décalé de façon différente entre les différents utilisateurs de l'Hololens. Cela peut probablement s'expliquer en partie par le fait que ce casque n'a pas une position universelle chez tous les utilisateurs et même entre deux sessions d'un même utilisateur. 


## Conclusion

Le projet a pu être réalisé avec un décalage spatial et temporel qui reste faible. Il permet d'ouvrir un champ de possibles multiples concernant les modalités d'interaction Humain-Robot. 

Des différentes visualisations créées, les polytopes ne semblent pas être la modalité visuelle la plus instinctive et compréhensible par un agent humain, dans le cadre d'une interaction sur l'intention de mouvement du robot. 

Cependant, il serait intéressant de réaliser un protocole d'étude pour déterminer objectivement la conséquence de ce type d'interaction en terme d'efficacité de tâche, et la modalité optimale pour une compréhension facile et rapide. 


## Perspectives 

Ce projet permet à l'équipe de pouvoir associer la visualisation des polytopes en réalité augmentée à leurs projets sur Panda. Il permettra de pouvoir diversifier les modalités de visualisations et de pouvoir les évaluer. 

## Références 

[1] Max Pascher, Uwe Gruenefeld, Stefan Schneegass, and Jens Gerken. How to Communicate Robot Motion Intent : A Scoping Review. In Proceedings of the 2023 CHI Conference on Human Factors in Computing Systems, pages 1–17, Hamburg Germany, April 2023. ACM.

[2] Gregory Lemasurier, Gal Bejerano, Victoria Albanese, Jenna Parrillo, Holly A. Yanco, Nicholas Amerson, Rebecca Hetrick, and Elizabeth Phillips. Methods for Expressing Robot Intent for Human–Robot Collaboration in Shared Workspaces. ACM Transactions on Human-Robot Interaction, 10(4) :40 :1–40 :27, September 2021.

[3] Daniel Szafir, Bilge Mutlu, and Terry Fong. Communicating Directionality in Flying Robots. In Proceedings of the Tenth Annual ACM/IEEE International Conference on Human-Robot Interaction, pages 19–26, Portland Oregon USA, March 2015. ACM.

[4] Tim Wengefeld, Dominik Höchemer, Benjamin Lewandowski, Mona Köhler, Manuel Beer, and Horst-Michael Gross. A Laser Projection System for Robot Intention Communication and Human Robot Interaction. In 2020 29th IEEE International Conference on Robot and Human Interactive Communication (RO-MAN), pages 259–265, August 2020. ISSN : 1944-9437.

[5] Ravi Teja Chadalavada, Henrik Andreasson, Robert Krug, and Achim J. Lilienthal. That’s on my mind ! robot to human intention communication through on-board projection on shared floor space. In 2015 European Conference on Mobile Robots (ECMR), pages 1–6, Lincoln, September
2015. IEEE.

[6] Atsushi Watanabe, Tetsushi Ikeda, Yoichi Morales, Kazuhiko Shinozawa, Takahiro Miyashita, and Norihiro Hagita. Communicating robotic navigational intentions. In 2015 IEEE/RSJ International Conference on Intelligent Robots and Systems (IROS), pages 5763–5769, 2015.

[7] Nicholas J. Hetherington, Elizabeth A. Croft, and H.F. Machiel Van Der Loos. Hey Robot, Which Way Are You Going ? Nonverbal Motion Legibility Cues for Human-Robot Spatial Interaction. IEEE Robotics and Automation Letters, 6(3) :5010–5015, July 2021.

[8] Antti Hietanen, Alireza Changizi, Minna Lanz, Joni Kamarainen, Pallab Ganguly, Roel Pieters, and Jyrki Latokartano. Proof of concept of a projection-based safety system for human-robot collaborative engine assembly. In 2019 28th IEEE International Conference on Robot and Human Interactive Communication (RO-MAN), pages 1–7, New Delhi, India, October 2019. IEEE.

[9] Eric Rosen, David Whitney, Elizabeth Phillips, Gary Chien, James Tompkin, George Konidaris, and Stefanie Tellex. Communicating Robot Arm Motion Intent Through Mixed Reality Head-Mounted Displays. In Nancy M. Amato, Greg Hager, Shawna Thomas, and Miguel Torres-Torriti, editors, Robotics Research, Springer Proceedings in Advanced Robotics, pages 301–316, Cham, 2020. Springer International Publishing.

[10] Mohammad-Ehsan Matour and Alexander Winkler. A multi-feature augmented reality-based robot control. In Tadej Petric, Ales Ude, and Leon Zlajpah, editors, Advances in Service and Industrial Robotics, pages 106–114, Cham, 2023. Springer Nature Switzerland.

[11] Uwe Gruenefeld, Lars Pr ̈adel, Jannike Illing, Tim Stratmann, Sandra Drolshagen, and Max Pfing-sthorn. Mind the arm : Realtime visualization of robot motion intent in head-mounted augmented
reality. In Proceedings of Mensch Und Computer 2020, MuC ’20, page 259–266, New York, NY, USA, 2020. Association for Computing Machinery.

[12] Georgios Tsamis, Georgios Chantziaras, Dimitrios Giakoumis, Ioannis Kostavelis, Andreas Kargakos, Athanasios Tsakiris, and Dimitrios Tzovaras. Intuitive and safe interaction in multi-user human robot collaboration environments through augmented reality displays. In 2021 30th IEEE
International Conference on Robot Human Interactive Communication (RO-MAN), pages 520–526, 2021

[13] Andre Cleaver, Darren Vincent Tang, Victoria Chen, Elaine Schaertl Short, and Jivko Sinapov. Dynamic Path Visualization for Human-Robot Collaboration. In Companion of the 2021 ACM/IEEE International Conference on Human-Robot Interaction, HRI ’21 Companion, pages 339–343, New York, NY, USA, March 2021. Association for Computing Machinery.

[14] Mark Zolotas, Murphy Wonsick, Philip Long, and Ta ̧skın Padır. Motion Polytopes in Virtual Reality for Shared Control in Remote Manipulation Applications. Frontiers in Robotics and AI, 8, 2021.

[15] Michael Walker, Hooman Hedayati, Jennifer Lee, and Daniel Szafir. Communicating Robot Motion Intent with Augmented Reality. In Proceedings of the 2018 ACM/IEEE International Conference on Human-Robot Interaction, HRI ’18, pages 316–324, New York, NY, USA, February 2018.
Association for Computing Machinery.

[16] Chrisantus Eze and Christopher Crick. Enhancing Human-robot Collaboration by Exploring Intuitive Augmented Reality Design Representations. In Companion of the 2023 ACM/IEEE International Conference on Human-Robot Interaction, HRI ’23, pages 282–286, New York, NY, USA, March 2023. Association for Computing Machinery.

[17] Antun Skuric, Vincent Padois, and David Daney. Approximating robot reachable space using convex polytopes. In 15th International Workshop on Human-Friendly Robotics, Delft, Netherlands, September 2022.
