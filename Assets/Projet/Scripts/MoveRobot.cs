using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.SampleQRCodes;

public class MoveRobot : MonoBehaviour
{
    public string qrCodeTag = "QrCode"; // Le tag � attribuer aux clones du qrCodeObject
    public ArticulationBody baseLink;
    private Vector3 RobotTranslation = new Vector3(-0.305f, 0f, -0.22f);
    //private Vector3 RobotTranslation = new Vector3(-0.310349f, 0f, -0.211546f); //(-y,z,x)
    //public Vector3 RobotTranslation = new Vector3(-0.31f, 0.22f, 0f); //(y,x,z)
    private Quaternion RobotOrientation = new Quaternion(-0f, 0.008354f, -0f, 0.999965f); //x,y,z,w (y, -z, -x, w)


    private Vector3 targetPosition; // Variable pour stocker la position cible
    private Quaternion targetRotation; // Variable pour stocker la rotation cible

    // Start est appel�e avant la premi�re mise � jour de frame
    void Start()
    {

    }

    // Update est appel�e une fois par frame
    void Update()
    {
        GameObject[] qrCodeObjects = GameObject.FindGameObjectsWithTag(qrCodeTag);

        foreach (GameObject qrCodeObject in qrCodeObjects)
        {
            // Obtenir la position et la rotation du QR Code
            Vector3 qrCodePosition = qrCodeObject.transform.position;
            Quaternion qrCodeRotation = qrCodeObject.transform.rotation;

            // Appliquer la position et la rotation au GameObject actuel
            transform.position = qrCodePosition;
            transform.rotation = qrCodeRotation * RobotOrientation;

            // Appliquer les rotations suppl�mentaires au GameObject pour que le robot soit perpendiculaire au QRCode 
            transform.Rotate(new Vector3(90f, 0f, 0f));
            transform.Rotate(new Vector3(0f, 180f, 0f));

            // Ajouter la translation du QRCode au robot
            //Vector3 transformedTranslation = RobotOrientation * RobotTranslation;
            //Vector3 targetPosition = qrCodePosition + transform.TransformDirection(transformedTranslation);
            targetPosition = qrCodePosition + transform.TransformDirection(RobotTranslation);
            //targetRotation = transform.rotation * RobotOrientation;

            // D�placer l'int�gralit� du robot
            baseLink.TeleportRoot(targetPosition, transform.rotation);

            //baseLink.transform.rotation = RobotOrientation;

        }
    }

    // M�thode publique pour acc�der � la position cible
    public Vector3 GetTargetPosition()
    {
        return targetPosition;
    }

    // M�thode publique pour acc�der � la rotation cible
    public Quaternion GetTargetRotation()
    {
        return transform.rotation;
    }
}
