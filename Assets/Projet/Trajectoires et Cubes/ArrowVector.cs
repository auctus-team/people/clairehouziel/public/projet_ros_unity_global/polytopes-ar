using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowVector : MonoBehaviour
{
    private VectorVelocityEffectorSubscriber vectorVelocityEffectorSubscriber;


    // Start is called before the first frame update
    void Start()
    {
        vectorVelocityEffectorSubscriber = FindObjectOfType<VectorVelocityEffectorSubscriber>();

    }

    // Update is called once per frame
    void Update()
    {

        //on r�cup�re les derni�res donn�es du subscriber � /velocity_effector
        Vector3 linearVelocity = vectorVelocityEffectorSubscriber.LinearVelocity;
        Vector3 angularVelocity = vectorVelocityEffectorSubscriber.AngularVelocity;
        Vector3 targetPosition = Vector3.zero;

        //si la valeur est inf�rieure � 1e-4, la valeur cible est � 0 et sinon elle est multipli�e par 3
        for (int i = 0; i < 3; i++)
        {
            if (Mathf.Abs(linearVelocity[i]) < 1e-4f)
            {
                targetPosition[i] = 0f;
            }
            else
            {
                targetPosition[i] = 3f * linearVelocity[i];
            }


            transform.localPosition = targetPosition;
            //Debug.Log(transform.position, transform.rotation);
        }


    }
}
